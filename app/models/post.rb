# == Schema Information
#
# Table name: posts
#
#  id          :integer          not null, primary key
#  title       :string
#  content     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rating      :string
#  slug        :string
#  category_id :integer
#

class Post < ApplicationRecord
    belongs_to          :category

    before_validation   :check_slug
    before_save         :titlize_title

    validates           :title,       presence: true
    validates           :slug,        uniqueness: true
    validates           :content,     presence: true
    validates           :rating,      numericality: true

    private
    def check_slug
      self.slug = self.title.parameterize
    end

    def titlize_title
      self.title = self.title.titleize
    end
end
