class PagesController < ApplicationController
  def index
    @today = Time.now
  end

  def contact
    @message = 'AB17 - Ruby on Rails 101 Sınıfı'
  end

  def contact_post
    @params = params['/contact']

    render "pages/contact"
  end
end
