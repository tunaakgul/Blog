# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts 'Categories seed...'
Category.create(name: 'Genel', description: 'Herşey burada!')
Category.create(name: 'Ruby on Rails', description: 'Ruby on Rails hakkında tuttuğum notlar')
Category.create(name: 'Ruby', description: 'Ruby hakkında tuttuğum notlar')
puts 'Categories seeded...'

puts 'Posts seed...'
Post.create(title: 'Ruby on Rails 101', content: 'Ruby on Rails\'e Giriş', rating: '5', slug: 'ruby-on-rails-101')
Post.create(title: 'Ruby on Rails Atölye', content: 'Ruby on Rails ile Proje Geliştirme Atölyesi', rating: '5', slug: 'ruby-on-rails-atolye')
Post.create(title: 'Ruby 101', content: 'Ruby programlama diline giriş', rating: '5', slug: 'ruby-101')
puts 'Posts seeded...'
